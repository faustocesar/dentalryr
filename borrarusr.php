<!DOCTYPE HTML5>
<html>
    <head>
        <title>R&R Consultorio Dental | Login</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="logStyle.css" type="text/css">
         <!-- script plugin facebook-->
        <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=730714503635309";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        <!--fin del Script--> 
        <link href='http://fonts.googleapis.com/css?family=Text+Me+One' rel='stylesheet' type='text/css'>
    </head>
    
    <body>
        <div id="layout">
            <div id="menup">
                <a href="index.html"><div id="logo" title="R&R | Su salud en nuestras manos... Amor por nuestro trabajo"></div></a>
                <ul id="redes">
                    <li id="facebook"><a href="#" title="Sígueme en Facebook"></a></li>
                    <li id="twitter"><a href="#" title="Sígueme en Twitter"></a> </li>
                    <li id="googleplus"><a href="#" title="Sígueme en Google +"></a></li>
                    <li id="tumblr"><a href="#" title="Visitame en Tumblr"></a></li>
                    <li id="pinterest"><a href="#" title="Pint this"></a></li>
                </ul> 
                    <div class="fb-like" data-href="https://www.facebook.com/cenesmat" data-width="40" data-layout="button" data-action="like" data-show-faces="true" data-share="true" title="Dale like al facebook!"></div>
            </div>
            <div id="linea"></div>
            <div id="menu">
            <menu>
                    <a href="index.html"><li>| Inicio |</li></a>
                    <a href="#about"><li>| Sobre nosotros |</li></a>
                    <a href="admin.php" class="activo"><li>| Admin |</li></a>
                    <a href="loginUsr.php"><li>| Usuario |</li></a>
                </menu>
        </div>
            <?php
//creamos la sesion
session_start();
//validamos si se ha hecho o no el inicio de sesion correctamente
//si no se ha hecho la sesion nos regresará a loginAdmin.php
if(!isset($_SESSION['usuario'])) 
{
  header('Location: loginAdmin.php'); 
  exit();
}
 ?>
  <div id="cajalog">BIENVENIDO, 
    <a href="logout.php">Cerrar Sesión</a>
 </div>
    <div id="corpus">
                        <table align="center">
                <tr>
                <td>
                <a class="boton" title="registrar" href="registro.php">Registrar Paciente</a>
                </td>
                <td>
                <a class="boton" title="agendar" href="agendar.php">Agendar Cita</a>
                </td>
                <td>
                <a class="boton" title="Borrar" href="borrarusr.php">Borrar Paciente</a>
                </td>

                </tr>
                    </table>
                <br>
        <br>
<?php
// conectamos a la base de datos
$link = mysqli_connect('localhost', 'u484039403_wm', '#MUld3r3x','u484039403_denta');
if(!$link) {
die("Error al intentar conectar: ".mysqli_error());
}
// hacemos una consulta
// para mostrar los registros
$sql = mysqli_query($link, "SELECT * FROM paciente") or die(mysqli_error());
// mostramos los registros
echo "<table align='center' id='tablaborrar'>";
echo "<tr>";
echo "<td><b>id</b></td>";
echo "<td><b>Nombre(s)</b></td>";
echo "<td><b>Apellido Paterno</b></td>";
echo "<td><b>Apellido Materno</b></td>";
echo "<td><b>Elminar</b></td>";
echo "</tr>";

while($row = mysqli_fetch_array($sql)){
echo "<tr>";
echo "<td>".$row['idpaciente']."</td>";
echo "<td>".$row['nombre']."</td>"; 
echo "<td>".$row['apPat']."</td>";
echo "<td>".$row['apMat']."</td>";
// mostramos un vinculo Eliminar
// que envia via $_GET
// el ID del registro a eliminar
echo"<td>"."<a href='eliminar.php?id=$row[idpaciente]'>Eliminar</a>"."\n"."</td>";
echo "</tr>";
}
echo "</table>";
?>
            </div>
        </div>
    </body>
</html>