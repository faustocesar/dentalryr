<!DOCTYPE HTML5>
<html>
    <head>
        <title>R&R Consultorio Dental | Login</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="logStyle.css" type="text/css">
         <!-- script plugin facebook-->
        <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=730714503635309";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        <!--fin del Script--> 
        <link href='http://fonts.googleapis.com/css?family=Text+Me+One' rel='stylesheet' type='text/css'>
    </head>
    
    <body>
        <div id="layout">
            <div id="menup">
                <a href="index.html"><div id="logo" title="R&R | Su salud en nuestras manos... Amor por nuestro trabajo"></div></a>
                <ul id="redes">
                    <li id="facebook"><a href="#" title="Sígueme en Facebook"></a></li>
                    <li id="twitter"><a href="#" title="Sígueme en Twitter"></a> </li>
                    <li id="googleplus"><a href="#" title="Sígueme en Google +"></a></li>
                    <li id="tumblr"><a href="#" title="Visitame en Tumblr"></a></li>
                    <li id="pinterest"><a href="#" title="Pint this"></a></li>
                </ul> 
                    <div class="fb-like" data-href="https://www.facebook.com/cenesmat" data-width="40" data-layout="button" data-action="like" data-show-faces="true" data-share="true" title="Dale like al facebook!"></div>
            </div>
            <div id="linea"></div>
            <div id="menu">
            <menu>
                    <a href="index.html"><li>| Inicio |</li></a>
                    <a href="#about"><li>| Sobre nosotros |</li></a>
                    <a href="acceso.php" class="activo"><li>| Acceso |</li></a>
                    <a href="#contact"><li>| Contacto |</li></a>
                </menu>
        </div>
            <?php
//creamos la sesion
session_start();
//validamos si se ha hecho o no el inicio de sesion correctamente
//si no se ha hecho la sesion nos regresará a loginAdmin.php
if(!isset($_SESSION['usuario'])) 
{
  header('Location: loginAdmin.php'); 
  exit();
}
 ?>
  <div id="cajalog">BIENVENIDO, 
    <a href="logout.php">Cerrar Sesión</a>
 </div>
    <div id="corpus">
                        <table align="center">
                <tr>
                <td>
                <a class="boton" title="registrar" href="registro.php">Registrar Paciente</a>
                </td>
                <td>
                <a class="boton" title="modificar" href="modificar.php">Modificar Paciente</a>
                </td>
                <td>
                <a class="boton" title="Borrar" href="borrarusr.php">Borrar Paciente</a>
                </td>

                </tr>
                    </table>
                <br>
        <br>
<?php
// conectamos a la base de datos
$link = mysqli_connect('localhost', 'u484039403_wm', '#MUld3r3x', 'u484039403_denta');
if(!$link) {
die("Error al intentar conectar: ".mysqli_error());
}
//fin conexion 

// comprobamos si ha sido enviado el formulario
if(isset($_POST['eliminar']) && $_POST['eliminar'] == 'Eliminar'){
// creamos la consulta que eliminara el registro
// que viene via $_POST
$id_eliminar = $_POST['id'];
$sqlEliminar = mysqli_query($link, "DELETE FROM paciente
WHERE idpaciente = $id_eliminar")
or die(mysqli_error());
// enviamos un mensaje de exito
$mensaje =  "El registro ha sido eliminado";
     header("location: borrarusr.php");
}
// si no ha sido enviado el formulario aun
// recogemos el ID del registro a eliminar via $_GET
elseif(isset($_GET['id'])){
$id = $_GET['id'];
// hacemos una consulta
// para mostrar el registro
// que vamos a eliminar
$sql = mysqli_query($link, "SELECT * FROM paciente
WHERE idpaciente = $id")
or die(mysqli_error());
$row = mysqli_fetch_array($sql);
// advertimos por el usuario que es medio #$%&
$mensaje =  "¿Está seguro que quiere eliminar el usuario <b>$row[nombre] $row[apPat] $row[apMat]</b>?";
}
// mostramos el mensaje
echo "<div id='meninf'>". $mensaje;
?>
<!-- creamos el formulario HTML
que enviara el ID
del registro a eliminar  -->
<form name="eliminar-registro" method="post" action="<?php $_SERVER['PHP_SELF']; ?>" >
<input name="id" type="hidden" value="<?php echo $row['idpaciente']; ?>" />
<input name="eliminar" type="submit" value="Eliminar" class="bot" />
</form>
</div>  
            </div>
        </div>
    </body>
</html>
